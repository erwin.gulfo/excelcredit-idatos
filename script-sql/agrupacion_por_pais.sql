			insert into
            agrupacion_por_pais(
                                pais,
                                cant_tratados_vigentes,
                                cant_tratados_no_vigentes,
                                fecha_adopcion_primer_tratado,
                                dif_meses
            )
			select x.estados_organismos,
            tratados_vigentes,
            tratados_no_vigentes,
            min(fecha_primer_tratado) primer_tratado,
            dif_meses
 				from(
				  select distinct t.estados_organismos,
                  case when (t.vigente=true) then
                  count(t.estados_organismos) end
				  tratados_vigentes,
                  case when (t.vigente=false) then
                  count(t.estados_organismos) end
				  tratados_no_vigentes,
                  min(t.fecha_adopcion) fecha_primer_tratado,
                  (current_date-(min(t.fecha_adopcion)))/30 dif_meses
				  from tratados t
				  where t.naturaleza_tratado='TRATADO'
				  group by t.estados_organismos,t.vigente,t.fecha_adopcion) x
        group by x.estados_organismos,
                 tratados_vigentes,
                 tratados_no_vigentes,
                 fecha_primer_tratado,
                 dif_meses